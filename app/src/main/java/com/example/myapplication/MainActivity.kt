package com.example.myapplication
/*I agree and accept to use this Free software/Opensource for
Capgemini Internal and Testing (of the Free software/Opensource software itself)
Purpose only and I need to take approval from Capgemini Legal and in particular
 the Local IP Officer when there is one in my country, for any Customer
 Deliverable/Production usage.
true
I agree and accept to use the Client Access License Software (CAL SW)/Client Software
for connecting to customer networks and I take complete responsibility for the
software license compliance of this CAL SW/Client Software installed on
my Capgemini PC.*/
import android.Manifest.permission



import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import android.Manifest.permission.RECORD_AUDIO

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager

import androidx.core.app.ActivityCompat
import android.Manifest.permission.RECORD_AUDIO

import androidx.core.content.ContextCompat

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.media.MediaRecorder
import android.view.View
import android.os.Environment
import android.util.Log
import java.io.IOException
import java.lang.IllegalStateException





import android.Manifest
import android.content.Context
import android.media.MediaPlayer


import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.LinearLayout
import com.amazonaws.auth.BasicAWSCredentials

import java.io.File
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain

import com.amazonaws.ClientConfiguration
import com.amazonaws.mobile.client.AWSMobileClient

import com.amazonaws.services.s3.AmazonS3


import com.amazonaws.services.s3.AmazonS3Client

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState

import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amplifyframework.AmplifyException
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.core.Amplify
import com.amplifyframework.predictions.aws.AWSPredictionsPlugin
import com.amplifyframework.storage.s3.AWSS3StoragePlugin
import java.lang.Exception
import java.util.*
import android.os.AsyncTask
import android.os.Handler


private const val LOG_TAG = "AudioRecordTest"
private const val REQUEST_RECORD_AUDIO_PERMISSION = 200
var awsAccessKey = "AKIATZ5OV4MBJVTZHPXP"
val awsSecretKey= "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"

class MainActivity : AppCompatActivity() {
    var notdone=true
    private var fileName: String = ""

    private var recordButton: RecordButton? = null
    private var recorder: MediaRecorder? = null
    private var transcribeButton : TranscribeButton? = null
    private var playButton: PlayButton? = null
    private var player: MediaPlayer? = null

    // Requesting permission to RECORD_AUDIO
    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)
    fun downloadInback(){
        val handler = Handler()
        handler.postDelayed({
            pollForTranscribeResult()
        }, 5000)

    }
    fun pollForTranscribeResult()
    {    val sharedPreference =  getSharedPreferences("JOB_ID",Context.MODE_PRIVATE)
        val key = sharedPreference.getString("jobID","ExampleKey")
        Log.d("key",key!!)


        val file = File("${applicationContext.filesDir}/download.json")
        Amplify.Storage.downloadFile(key, file,
            { Log.i("MyAmplifyApp", "Successfully downloaded: ${it.file.name}") },
            { Log.e("MyAmplifyApp",  "Download Failure", it) }
        )
    }
    fun saveToSharedPref(job_id : String)
    {
        val sharedPreference =  getSharedPreferences("JOB_ID",Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString("jobID",job_id)

        editor.commit()
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!permissionToRecordAccepted) finish()
    }

    private fun onRecord(start: Boolean) = if (start) {
        startRecording()
    } else {
        stopRecording()
    }

    private fun onPlay(start: Boolean) = if (start) {
        startPlaying()
    } else {
        stopPlaying()
    }

    private fun startPlaying() {
        player = MediaPlayer().apply {
            try {
                setDataSource(fileName)
                prepare()
                start()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }
        }
    }

    private fun stopPlaying() {
        player?.release()
        player = null
    }

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            setAudioSamplingRate(44100)// default is 8k. Not supported by transcribe.
            setAudioEncodingBitRate(38400)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            start()
        }

    }

    private fun stopRecording() {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    private fun uploadtoS3v4() {
        val exampleFile = File(fileName)
        val uuid: UUID = UUID.randomUUID()
        val uniqueJobId= "ExampleKey_${uuid}"
        Log.d("jobid",uniqueJobId)
        saveToSharedPref(uniqueJobId)
        Amplify.Storage.uploadFile(uniqueJobId, exampleFile,
            { Log.i("MyAmplifyApp", "Successfully uploaded: ${it.key}") },
            { Log.e("MyAmplifyApp", "Upload failed", it) }
        )



    }

/*    private fun uploadToS3() {

        AWSMobileClient.getInstance().initialize(this).execute()


        val credentials = BasicAWSCredentials(awsAccessKey, awsSecretKey)
        val s3Client = AmazonS3Client(credentials)

        val transferUtility = TransferUtility.builder()
            .context(applicationContext)
            .awsConfiguration(AWSMobileClient.getInstance().configuration)
            .s3Client(s3Client)
            .build()

// "jsaS3" will be the folder that contains the file

// "jsaS3" will be the folder that contains the file
        val uploadObserver = transferUtility.upload("anandhrjnuseast1", File(fileName))

        uploadObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                if (TransferState.COMPLETED === state) {
                    // Handle a completed download.
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                val percentDone = percentDonef.toInt()
            }

            override fun onError(id: Int, ex: Exception) {
                // Handle errors
            }
        })

// If your upload does not trigger the onStateChanged method inside your
// TransferListener, you can directly check the transfer state as shown here.

// If your upload does not trigger the onStateChanged method inside your
// TransferListener, you can directly check the transfer state as shown here.
        if (TransferState.COMPLETED === uploadObserver.state) {
            // Handle a completed upload.
        }
        }  */



  /*  private fun uploadToS3() {
        val credentials = BasicAWSCredentials(awsAccessKey, awsSecretKey)
        val transferManager = TransferManager(credentials)
        val upload = transferManager.upload("anandhrjnuseast1", "plswork", File(fileName))
        val uploadResult = upload.waitForUploadResult()

        while (!upload.isDone){
            val bytesTransferred = upload.progress.bytesTransferred
            Log.d("uploaded",bytesTransferred.toString())
            Thread.sleep(200)
        }
    }*/
  /*  private fun uploadtoS3v2()
    {    val credentials = BasicAWSCredentials(awsAccessKey, awsSecretKey)

        val s3: AmazonS3 = AmazonS3ClientBuilder.standard().withRegion("us-east-1")
            .withClientConfiguration(ClientConfiguration())
            .withCredentials(DefaultAWSCredentialsProviderChain()).build()
        val client: AmazonTranscribe =
            AmazonTranscribeClient.builder().withRegion("us-east-1").build()

        s3.putObject("anandhrjnuseast1", fileName,  File(fileName));

    }*/

    internal inner class RecordButton(ctx: Context) : androidx.appcompat.widget.AppCompatButton(ctx) {

        var mStartRecording = true

        var clicker: OnClickListener = OnClickListener {
            onRecord(mStartRecording)
            text = when (mStartRecording) {
                true -> "Stop recording"
                false -> "Start recording"
            }
            mStartRecording = !mStartRecording
        }

        init {
            text = "Start recording"
            setOnClickListener(clicker)
        }
    }

    internal inner class PlayButton(ctx: Context) : androidx.appcompat.widget.AppCompatButton(ctx) {
        var mStartPlaying = true
        var clicker: OnClickListener = OnClickListener {
            onPlay(mStartPlaying)
            text = when (mStartPlaying) {
                true -> "Stop playing"
                false -> "Start playing"
            }
            mStartPlaying = !mStartPlaying
        }

        init {
            text = "Start playing"
            setOnClickListener(clicker)
        }
    }

    internal inner class TranscribeButton(ctx: Context) : androidx.appcompat.widget.AppCompatButton(ctx) {

        var clicker: OnClickListener = OnClickListener {
        uploadtoS3v4()
            downloadInback()


        }

        init {
            text = "Transcribe"
            setOnClickListener(clicker)
        }
    }


    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)



        try {
            Amplify.addPlugin(AWSCognitoAuthPlugin())
            Amplify.addPlugin(AWSS3StoragePlugin())
            Amplify.addPlugin(AWSPredictionsPlugin())

            Amplify.configure(applicationContext)

            Log.i("MyAmplifyApp", "Initialized Amplify")
        } catch (error: AmplifyException) {
            Log.e("MyAmplifyApp", "Could not initialize Amplify", error)
        }

        // Record to the external cache directory for visibility
        fileName = "${externalCacheDir?.absolutePath}/audiorecordtest.mp4"

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

        recordButton = RecordButton(this)
        playButton = PlayButton(this)
        transcribeButton = TranscribeButton(this)
        val ll = LinearLayout(this).apply {
            addView(recordButton,
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0f))
            addView(playButton,
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0f))
            addView(transcribeButton,
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0f))
        }
        setContentView(ll)
    }

    override fun onStop() {
        super.onStop()
        recorder?.release()
        recorder = null
        player?.release()
        player = null
    }

    fun onStartBTN(view: View) {}
    fun onStopClicked(view: View) {}
}